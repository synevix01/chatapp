import { Flexbox, Button, Cage, Container, Image, Paragraph } from "@synevix/react-widget";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import dayjs from "dayjs";
import CreateGroup from "../components/CreateGroup";
import { IChats, IUser } from "../utils/interface";
import { database, onValue, ref, update, onDisconnect, getAuth, signOut } from "../utils/firebase";
import Chat from "./Chat";
import ChatList from "./ChatList";
import Group from "./Group";
import Direct from "./Direct";
import User from "./User";
import logo from '../assets/logo.png';


const Dashboard = (props: IDashboard) => {

    const navigate = useNavigate();

    const [sideChat, setSideChat] = useState<boolean>(false);
    const [modal, setModal] = useState<boolean>(false);
    const [contextMenu, setContextMenu] = useState<boolean>(false);
    const [type, setType] = useState<string>('groups');
    const [data, setData] = useState<IUser>({} as IUser);
    const [chat, setChat] = useState<IChats>({} as IChats);


    useEffect(() => {

        document.getElementById('bodyHolder')?.addEventListener('click', (e) => setContextMenu(false));

        onValue(ref(database, ".info/connected"), (snap) => {
            if (snap.val() === true) {
                const updates: any = {};

                if (data!.uniqueID?.length! > 3) {
                    updates['/user/' + data.uniqueID + '/lastSeen'] = 'online';
                    update(ref(database), updates).catch(err => console.error(err));
                }
            }
        });

        return () => {
            document.getElementById('bodyHolder')?.removeEventListener('click', (e) => setContextMenu(false))
            if (data!.uniqueID?.length! > 3)
                onDisconnect(ref(database, '/user/' + data.uniqueID)).update({ lastSeen: dayjs().format('YYYY-MM-DD') }).then(f => console.log(f)).catch(e => console.log(e));
        }
    }, [data]);


    useEffect(() => {
        onValue(ref(database, 'user'), (res: any) => {
            if (res.val() != null) {
                const result = Object.values(res.val()).find((item: any) => item.id === props.id) as IUser;
                setData(result);
            }
        })
    }, [props.id])


    const logOut = () => {

        signOut(getAuth()).then(() => {
            navigate({ pathname: '/login' }, { replace: true });
        }).catch((error) => {
            // An error happened.
        });
    }


    const onGroupClick = (name: string, uniqueID: string) => {
        setChat({ name, uniqueID, type: 'groups', goBack: (ard) => console.log(ard), username: '', role: data.role });
        setSideChat(true);

    }

    const onChatListClick = (name: string, uniqueID: string) => {

        // onValue(ref(database, 'direct/' + name + '/messages'), (res: any) => {


        //     if (res.val() != null) {
        //         Object.values(res.val()).forEach((item: any) => {
        //             if (item.name === 'admin') {
        //                 console.log(item);
        //                 const updates: any = {};
        //                 updates['/direct/' + name + '/messages/' + item.uniqueID + '/onRead'] = true;
        //                 update(ref(database), updates).then(() => console.log('Successful')).catch(err => console.log(err));
        //             }
        //         });

        //     }
        // })


        setChat({ name, uniqueID, type: 'chat', goBack: (ard) => console.log(ard), username: '', role: data.role });
        setSideChat(true);
    }

    const onDirectClick = (name: string, uniqueID: string) => {
        setChat({ name, uniqueID, type: 'direct', goBack: (ard) => console.log(ard), username: '', role: data.role });
        setSideChat(true);
    }

    const onBtnGroup = () => {
        setType('groups')
    }

    const onBtnDirect = () => {
        setType('direct');
    }


    const onBtnChat = () => {
        setType('chat');
    }

    const onBtnUser = () => {
        setType('user')
    }

    const Navbar = () => (
        <Cage className="lg:w-1/12  lg:h-screen ">
            <Cage className=" bg-gray-800 flex flex-col justify-between  shadow-md h-full">
                <Container className="flex justify-between px-2 pt-3">
                    <div>
                        <Image source={logo} alt={'mypowerhouse'} className={'w-12 lg:w-24 lg:mx-auto inline-block mr-2 lg:pr-0'} />
                        <Paragraph text={'My PowerHouse'} className={'inline text-white'} />
                    </div>

                    {/* <Cage className=" lg:w-20 lg:h-20 lg:my-4 lg:border-4 rounded-full lg:mx-auto flex items-center p-2 divide-white">

                        <Paragraph text={'mPh'} className="logo_text text-2xl font-bold text-white" />
                    </Cage> */}
                    <Cage className="relative lg:sr-only">
                        <Button onClick={() => setContextMenu((prev) => !prev)} icon="icon-more" iconSize="20px" className="text-white bg-transparent " />
                        <div className={`${contextMenu ? 'null' : 'hidden'}    bg-gray-50 w-max p-3 absolute right-0 shadow-md z-20 rounded`}>
                            {
                                data.role === 'admin' && <Button icon="icon-plus3" text="Create Group" onClick={() => setModal(true)} className={'bg-transparent  mb-3 w-full'} />

                            }
                            <Button icon="icon-switch2" text="Logout" onClick={logOut} className={'bg-transparent  w-full text-left'} style={{ display: 'block' }} />
                        </div>
                        <CreateGroup modal={modal} setModal={(args: boolean) => setModal(args)} />
                    </Cage>

                </Container>
                <Flexbox className="lg:flex-col text-white gap-6 py-1">
                    <Button icon="icon-users" text="Group" onClick={onBtnGroup} className={`bg-transparent  ${type === 'groups' && 'border-b lg:bg-gray-500 '}  mx-auto p-2 `} />
                    {
                        data.role !== 'admin' &&
                        <Button icon="icon-user" text="Direct" onClick={onBtnDirect} className={`bg-transparent  ${type === 'direct' && 'border-b  lg:bg-gray-500'}   mx-auto p-2`} />
                    }
                    {
                        data.role === 'admin' &&
                        <Button icon="icon-user" text="Chat" onClick={onBtnChat} className={`bg-transparent  ${type === 'chat' && 'border-b lg:bg-gray-500'}   mx-auto p-2`} />
                    }
                    {
                        data.role === 'admin' &&
                        <Button icon="icon-address-book" text="User" onClick={onBtnUser} className={`bg-transparent  ${type === 'user' && 'border-b lg:bg-gray-500'}   mx-auto p-2`} />
                    }
                </Flexbox>
                <Cage className="sr-only lg:not-sr-only  flex justify-center">
                    <Button onClick={logOut} icon="icon-switch2" iconSize="24px" className="text-white my-3 bg-transparent " />
                </Cage>
            </Cage>
        </Cage>

    );




    return (
        <Cage className=" flex lg:flex-row flex-col h-screen ">
            <Navbar />
            <Cage className="lg:w-3/12 lg:h-screen flex-1 " id="bodyHolder">
                {
                    type === 'groups'
                        ? <Group onGroupClick={(name, uniqueID, type) => onGroupClick(name, uniqueID)} role={data.role} />
                        : type === 'direct'
                            ? <Direct onDirectClick={(name, uniqueID, type) => onDirectClick(name, uniqueID)} username={data.username} />
                            : type === 'chat'
                                ? <ChatList onChatListClick={(name, uniqueID, type) => onChatListClick(name, uniqueID)} role={data.role} />
                                : <User onUserClick={(name, uniqueID, type) => { }} username={data.username} />
                }
            </Cage>
            <Cage className={`lg:w-8/12 lg:h-screen  lg:not-sr-only  ${sideChat ? ' absolute inset-0 not-sr-only z-40' : 'null sr-only '}`} id={'chat-book'}>
                <Chat name={chat.name} uniqueID={chat.uniqueID} goBack={(args) => { setSideChat(args); }} type={chat.type} username={data.role === 'admin' ? 'admin' : data.username} role={data.role} />
            </Cage>
        </Cage>

    )



}

interface IDashboard {
    id: string;
}

export default Dashboard;