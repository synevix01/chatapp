import { Flexbox, TextField, Cage, Icon } from "@synevix/react-widget";
import { useEffect, useState } from "react";
import { IChat, IMessagebox } from "../utils/interface";
import Picker from 'emoji-picker-react';

const Messagebox = (props: IMessagebox) => {
    const [chat, setChat] = useState<IChat>({} as IChat);
    const [emoji, setEmoji] = useState<boolean>(false);



    useEffect(() => {
        document.getElementById('bodyHolder')?.addEventListener('click', (e) => setEmoji(false));
        document.getElementById('chat-holder')?.addEventListener('click', (e) => setEmoji(false));

        return () => {
            document.getElementById('chat-holder')?.removeEventListener('click', () => null);
        }

    })

    useEffect(() => {
        setChat({ ...chat, reply: props.reply.message, replyUser: props.reply.user });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.reply.message, props.reply.user]);


    const btnSendMessage = () => {
        if (chat!.message!.length > 0) {
            props.onSend(chat);
            props.onReplyAction(false)
            setChat({} as IChat);
        }
        setChat({} as IChat)
    }

    const onTextChange = (e: any) => {
        setChat({
            ...chat,
            message: e.target.value,
            user: props.username,
            timestamp: new Date().toLocaleString('en-GB'),
            onRead: false,
            reply: props.reply.message,
            replyUser: props.reply.user  
        })
    }

    const onEmojiClick = (_event: any, _emojiObject: any) => {
        setChat({
            ...chat,
            message: chat.message + _emojiObject.emoji,
            user: props.username,
            timestamp: new Date().toLocaleString('en-GB'),
            onRead: false
        })

    };
 
    const onBtnCloseReply = () => {
        props.onReplyAction(false)

    }

    return (
        <Cage className="">
            {
                emoji && <Picker onEmojiClick={onEmojiClick} pickerStyle={{ position: 'fixed', bottom: '70px' }} />
            }
            {
                props.reply.status && (
                    <div className="bg-white absolute border border-t-2 border-t-gray-300 w-12/12  bottom-14 inset-x-0 lg:left-1/3  p-3 truncate   ">
                        <span className="icon-cross text-right block cursor-pointer " onClick={onBtnCloseReply}></span>
                        <p className="truncate">{props.reply.message}</p>
                    </div>
                )
            }

            <Cage className="bg-white rounded-full">
                <Flexbox className={'fixed lg:static bottom-0 px-2 py-2 inset-x-1 lg:mx-2  border rounded-full  gap-4'}>
                    <button onClick={() => setEmoji(prev => !prev)} className={'hover:bg-gray-500 rounded-full  bg-gray-600 text-white   w-10 h-10  text-center flex justify-center  items-center'}>
                        <Icon name="icon-ticket" className="text-lg" />
                    </button>
                    {/* <button onClick={() => setEmoji(prev => !prev)} className={'hover:bg-gray-500 rounded-full  bg-gray-600 text-white   w-10 h-10  text-center flex justify-center  items-center'}>
                    <Icon name="icon-plus3" className="text-lg" />
                </button> */}
                    <TextField placeholder={'Type Message'} value={chat.message} onValueChange={onTextChange} className={'border-none flex-1'} />
                    <button onClick={btnSendMessage} className={'hover:bg-blue-500 rounded-full button-bg text-white   w-10 h-10  text-center flex justify-center  items-center'}>
                        <Icon name="icon-paperplane" className="text-lg" />
                    </button>

                </Flexbox>
            </Cage>
        </Cage>

    )
}


export default Messagebox;