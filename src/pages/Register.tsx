import { Button, Cage, Grid, Paragraph, TextField, Image } from "@synevix/react-widget";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import register from '../assets/register.svg';
import { database, push, ref, set } from "../utils/firebase";
import { IUser, IError } from "../utils/interface";
import { textboxFilter, isEmpty, noSpace, checkLength, alphaNumber, existence } from "../utils/textboxFilter";
import logo from '../assets/logo.png';

const Register = () => {

    const [user, setUser] = useState<IUser>({} as IUser);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [error, setError] = useState<IError>({ status: false, message: '' });
    const history = useNavigate();
    const auth = getAuth();


    const newUser = () => {
        setIsLoading(true);
        setError({ status: false, message: '' });
        const uniqueID = push(ref(database, `user`)).key;
        user.role = 'guest';
        user.uniqueID = uniqueID // Change to users after everything
        user.dateCreated = new Date().toLocaleString('en-GB');
        user.lastSeen = new Date().toLocaleString('en-GB');


        textboxFilter(user.username)
            .then(e => isEmpty(e))
            .then(e => noSpace(e))
            .then(e => checkLength(e))
            .then(e => alphaNumber(e))
            .then(e => existence(e))
            .then(e => {
                createUserWithEmailAndPassword(auth, user.username + '@mph.com', user.passcode)
                    .then((userCredential) => {

                        user.id = userCredential.user.uid;
                        set(ref(database, 'user/' + uniqueID), { ...user });
                        history({ pathname: '/dashboard' });
                    })
                    .catch((error) => {

                        //const errorMessage = error.message;
                        setError({ status: true, message: error.code });
                        setIsLoading(false);
                    });
            })
            .catch(err => {
                setError({ status: true, message: err })
                setIsLoading(false);
            });

    }



    return (
        <Grid className="bg-blue-100 min-h-screen" lg="2" gap="4">
            <Cage className="sr-only lg:not-sr-only  h-full">
                <Link to={'/'} className={'p-6   inline-block '}>
                    <img src={logo} alt={'logo'} className={'w-20 mx-auto'} />
                    <Paragraph text={'My PowerHouse'} className={'font-bold text-xl text-gray-800'} />
                </Link>
                <img src={register} alt="register" className=" w-9/12 mx-auto" />
            </Cage>
            <Cage className="py-8 bg-gray-700 flex flex-col justify-center px-4 md:px-12" >
                <Image source={logo} alt={'mypowerhouse'} className={'mx-auto'} width={'150px'} />

                <Paragraph text={'My PowerHouse'} className="my-4 text-xl font-bold text-white text-center" />
                <Grid gap="6" className="">
                    <Cage className="lg:w-1/2 mx-auto w-full">
                        <Paragraph text={'Username'} className="text-white mb-1" />
                        <TextField required={true} className="w-full" type="text" placeholder={'Enter username'} value={user.username} onValueChange={(e) => setUser({ ...user, username: e.target.value })} />

                    </Cage>
                    <Cage className="lg:w-1/2 mx-auto w-full">
                        <Paragraph text={'Password'} className="text-white mb-1" />
                        <TextField className="" type="password" showEye={true} placeholder={'Enter password'} value={user.passcode} onValueChange={(e) => setUser({ ...user, passcode: e.target.value })} />

                    </Cage>
                    <Paragraph text={error.message} className={`text-red-400 italic ${error.status || 'hidden'} lg:w-1/2 mx-auto w-full`} />
                    <Button isLoading={isLoading} onClick={newUser} text={'Create an account'} className="lg:w-1/2 mt-2 mx-auto w-full py-2 px-5 font-bold button-bg   text-white hover:bg-blue-500 outline-none" />
                    <p className={`text-white lg:w-1/2 mx-auto w-full`} >
                        Already have an account?
                        <Link to={'/login'} className="text-blue-400 pl-2">Login Here</Link>

                    </p>
                </Grid>



            </Cage>
        </Grid>
    )
}

export default Register;