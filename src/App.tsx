import { BrowserRouter as Router, Route, Routes, Navigate  } from 'react-router-dom'
import { useState } from 'react';
import { getAuth, onAuthStateChanged } from 'firebase/auth';
import '@synevix/react-widget/dist/esm/widget.css';
import 'simplebar/dist/simplebar.min.css';
import './tailwind.css';
import './App.css';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard'; 




function App() {
  const [loggedIn, setLoggedIn] = useState<boolean>(false); 
  const [id, setId] = useState<string>('')
  const auth = getAuth();
  onAuthStateChanged(auth, (user) => {
    if (user) {
      setLoggedIn(true) 
      setId(user.uid)
    } else {
      setLoggedIn(false)
      
    }
  });

  return (
    <Router> 
      <Routes>
        <Route path={'/'} element={<Home/>}/>
        <Route path={'/login'} element={<Login />} />
        <Route path={'/register'} element={<Register />} />
        <Route path={'/dashboard'} element={loggedIn ? <Dashboard id={id} /> :<Navigate to="/login" />} />  
      </Routes>
    </Router>
  );
}

 
export default App;
