import { Cage, Flexbox, Paragraph, Button, Avatar, Span, Icon, Grid, Label, TextField } from "@synevix/react-widget";
import { DatabaseReference, DataSnapshot } from "firebase/database";
import { Children, useEffect, useState } from "react";
import SimpleBar from 'simplebar-react';
import Datestamp from "../components/Datestamp";
import Messagebox from "../components/Messagebox";
import Modal from "../components/Modal";
import { database, onValue, push, ref, remove, update } from '../utils/firebase';
import { IChat, IChats, IReply } from "../utils/interface";

const Chat = (props: IChats) => {


    const [data, setData] = useState<any>({} as any);
    const [groupName, setGroupName] = useState<string>('')
    const [isFirstTime, setIsFirstTime] = useState<boolean>(false);
    const [isGroupRename, setGroupRename] = useState<boolean>(false);
    const [contextMenu, setContextMenu] = useState<number>(-1);
    const [reply, setReply] = useState<IReply>({ message: "", user: "", status: false });

    useEffect(() => {
        window.scrollTo(0, document.body.scrollHeight);
    })

    useEffect(() => {
        let startCountRef: DatabaseReference;
        if (props.type === 'groups') {
            startCountRef = ref(database, `groups/${props.uniqueID}/`);
        } else {
            startCountRef = ref(database, `direct/${props.name}/`);
        }

        onValue(startCountRef, (data: DataSnapshot) => {
            if (data.val() != null) {
                setData(data.val())
                setIsFirstTime(false);
            } else {
                setData({} as any);
                setIsFirstTime(true);
            }
        })


    }, [props.name, props.type, props.uniqueID])


    const onChatSend = (chat: IChat) => {

        let uniqueID: string | null = '';
        const updates: any = {};
        const userUpdate: any = {};

        if (props.type === 'groups') {
            uniqueID = push(ref(database, `groups/${props.uniqueID}/messages`)).key;
            chat.uniqueID = uniqueID!;
            if (props.role === 'admin') {
                chat.name = 'admin';
                chat.user = 'admin';
            }
            updates['/groups/' + props.uniqueID + '/messages/' + uniqueID] = { ...chat };
            update(ref(database), updates).then(() => console.log('Successful')).catch(err => console.log(err));
        } else if (props.type === 'direct') {
            uniqueID = push(ref(database, `direct/${props.uniqueID}/messages`)).key;
            chat.uniqueID = uniqueID!;
            chat.name = props.username;
            userUpdate['/direct/' + props.username + '/user'] = props.username;
            userUpdate['/direct/' + props.username + '/uniqueID'] = uniqueID;
            updates['/direct/' + props.username + '/messages/' + uniqueID] = { ...chat };
            update(ref(database), userUpdate).then(() => console.log('Successful')).catch(err => console.log(err));
            // Bot chat
            if (isFirstTime) {
                uniqueID = push(ref(database, `${props.type}/${props.uniqueID}/messages`)).key;
                chat.uniqueID = uniqueID!;
                chat.user = 'bot';
                chat.name = 'bot';
                chat.message = 'We are glad to have you here, \n how may I be of help?'
                updates['/direct/' + props.username + '/messages/' + uniqueID] = { ...chat };
                update(ref(database), updates).then(() => console.log('Successful')).catch(err => console.log(err));

            }

        } else {
            uniqueID = push(ref(database, `direct/${props.name}/messages`)).key;
            chat.uniqueID = uniqueID!;
            chat.name = 'admin';
            chat.user = 'admin';
            updates['/direct/' + props.name + '/messages/' + uniqueID] = { ...chat };
            update(ref(database), userUpdate).then(() => console.log('Successful')).catch(err => console.log(err));
        }




        update(ref(database), updates).then(() => console.log('mess')).catch(err => console.log(err));
        //window.scrollTo(0, document.body.scrollHeight);


    }

 
    const goBack = () => {
        props.goBack(false);
    }

    const onMessageDelete = (id: string) => {
        if (props.type === 'groups')
            remove(ref(database, `groups/${props.uniqueID}/messages/${id}`));
        else
            remove(ref(database, `direct/${props.name}/messages/${id}`));
    }

    const onMessageCopy = (text: string) => {

        navigator.clipboard.writeText(text).then(function () {
            console.log('Async: Copying to clipboard was successful!');
        }, function (err) {
            console.error('Async: Could not copy text: ', err);
        });
    }

    const onMessageReply = (text: string, user: string) => {
        setReply({ message: text, status: true, user: user });

    }

    const onGroupDelete = () => {
        remove(ref(database, `groups/${props.uniqueID}`));
        setData({} as any);
    }

    const onGroupRename = () => {
        const updates: any = {};
        updates['/groups/' + props.uniqueID + '/name'] = groupName;
        update(ref(database), updates).then(() => console.log('Successful')).catch(err => console.log(err));
        setGroupRename(false);
        setGroupName('')
    }

    return (
        <Cage className={'h-full '}>

            {
                props.uniqueID ?
                    (
                        <Cage className="flex flex-col h-min-screen  bg-white ">
                            <Cage className="p-3 bg-gray-800 text-white  ">
                                <Flexbox>
                                    <Button onClick={goBack} icon="icon-arrow-left8" iconSize="24px" className="lg:sr-only bg-transparent" />
                                    <Paragraph text={props.name === props.username ? 'Direct' : props.name} className="flex-1 text-center" style={{ justifySelf: 'center' }} />
                                    {
                                        props.role === 'admin' && props.type === 'groups' &&

                                        <Cage className="flex gap-3">
                                            <Button onClick={() => setGroupRename(true)} icon={'icon-pencil5'} className={'bg-transparent '} />
                                            <Button onClick={onGroupDelete} icon={'icon-trash'} tooltiptext={'Delete'} tooltippos={'left'} className={'bg-transparent '} />
                                            <Modal isOpen={isGroupRename} setOpen={() => setGroupRename(false)} title={'Rename Group'}  >
                                                <Grid gap="4">
                                                    <Label className="text-gray-600">Group Name</Label>
                                                    <TextField placeholder={'Group Name'} className={'text-black'} value={groupName} onValueChange={(e) => setGroupName(e.target.value)} />


                                                    <Button text={'Rename Group'} onClick={onGroupRename} className={'bg-gray-800 hover:bg-gray-700 text-white p-2'} />
                                                </Grid>
                                            </Modal>
                                        </Cage>

                                    }
                                </Flexbox>
                            </Cage>



                            <SimpleBar className="lg:chat-height  chat-height" style={{ maxHeight: '100%' }} id={'chat-holder'}>
                                {
                                    data!.messages && Children.toArray(Object.values(data!.messages!).map((item: any, index: any) => {
                                        return (
                                            <Cage className="px-3 h-full">
                                                <Datestamp index={index} previousDate={data!.messages!} currentDate={item.timestamp} />
                                                <div onClick={() => setContextMenu((prev) => prev === index ? -1 : index)} className={`${item.user !== props.username ? 'bg-blue-100' : 'bg-pink-100 ml-auto'} p-2 rounded-md width-fit  my-3 text-sm relative`}>

                                                    <Span className={'text-xs text-gray-500 italic '}>@{item.user}</Span>
                                                    {
                                                        item.reply &&
                                                        <div className="bg-gray-50 p-2 border-l-4 rounded-r-md ">
                                                            <p className="italic text-xs text-gray-400">@{item.replyUser}</p>
                                                            <p>{item.reply}</p>
                                                        </div>
                                                    }
                                                    <Paragraph text={item.message} className="chat_text mt-2 whitespace-pre-line" />
                                                    <Paragraph text={item.timestamp.match(/\s+\d{2}:\d{2}/gi)!.toString()} className="text-right text-xs text-gray-500" />

                                                    <div className={`${contextMenu === index ? 'null' : 'hidden'}  bg-gray-50 w-max p-3 absolute right-0 shadow-md z-30 rounded`}>
                                                        <div className="flex  ">
                                                            <Icon name="icon-copy3" className="float-right" />
                                                            <Button text="Copy" onClick={() => onMessageCopy(item.message)} className={'bg-transparent mb-3 w-full text-left px-1'} style={{ display: 'block' }} />
                                                        </div>
                                                        <div className="flex  ">
                                                            <Icon name="icon-undo2" className="float-right" />
                                                            <Button text="Reply" onClick={() => onMessageReply(item.message, item.user)} className={'bg-transparent mb-3 w-full text-left px-1'} style={{ display: 'block' }} />
                                                        </div>
                                                        {
                                                            props.username === item.user && (
                                                                <div className="flex  ">
                                                                    <Icon name="icon-trash" className="float-right" />
                                                                    <Button text="Delete" onClick={() => onMessageDelete(item.uniqueID)} className={'bg-transparent w-full text-left px-1'} style={{ display: 'block' }} />
                                                                </div>
                                                            )
                                                        }

                                                    </div>


                                                </div>
                                            </Cage>
                                        )
                                    }

                                    ))
                                }

                            </SimpleBar>

                            <Messagebox onSend={onChatSend} username={props.username!} reply={reply} onReplyAction={(arg0: boolean) => setReply({ message: '', status: arg0, user: '' })} />
                        </Cage>
                    ) : (
                        <Flexbox className="h-screen" justifyContent="center" alignItems="center" direction="col" gap="6">
                            <Avatar text={props.username && props!.username!.match(/\w{2}/gi)![0]} className="h-24 w-24 text-2xl capitalize" style={{ width: '6rem' }} />
                            <Paragraph text={`Welcome ${props.username}`} className="text-gray-600 font-bold text-2xl capitalize" />
                            <Paragraph text={'Start a conversation now'} className="text-gray-500 border-2 p-3" />
                        </Flexbox>

                    )
            }
        </Cage>
    )
}



export default Chat;