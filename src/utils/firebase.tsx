import {initializeApp} from 'firebase/app'
import {  getDatabase, ref, onValue, set, push, child, update, remove, onDisconnect} from 'firebase/database'
import { getAuth, signOut } from "firebase/auth";

const config = {
    apiKey: "AIzaSyBzG2vlGtoeOFZpmSCxBFYnQG37c8eTEsk",
    authDomain: "bellicare-48c30.firebaseapp.com",
    projectId: "bellicare-48c30",
    storageBucket: "bellicare-48c30.appspot.com",
    messagingSenderId: "186260108061",
    appId: "1:186260108061:web:284f10feea16c5838727f4",
    measurementId: "G-YXYXZRL21F"
};
initializeApp(config);
const database = getDatabase();

export {
    database,
    ref, 
    onValue,
    set,
    push,
    child,
    update,
    remove,
    onDisconnect,
    getAuth,
    signOut
} 