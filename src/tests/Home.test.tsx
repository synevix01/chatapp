import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import Home from '../pages/Home';

test('renders landing text', () => {
  render(
    <BrowserRouter >
      <Home />
    </BrowserRouter>);
  const linkElement = screen.getByText(/Share your emotions/i);
  expect(linkElement).toBeInTheDocument();
});  
