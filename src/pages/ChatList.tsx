import { Cage, Paragraph, Avatar, Flexbox } from "@synevix/react-widget";
import { Children, useEffect, useState } from "react";
import SimpleBar from 'simplebar-react';
import { database, onValue, ref } from '../utils/firebase';

const ChatList = (props: IChatList) => {

    const [data, setData] = useState<any[]>([]);



    useEffect(() => {

        onValue(ref(database, 'direct'), (res: any) => {
            if (res.val() != null) {
                setData([])
                Object.values(res.val()).forEach((item: any) => {
                    setData((prev) => [...prev, {
                        name: item.user,
                        uniqueID: item.uniqueID,
                        // unRead:  Object.values(item.messages).filter((dt:any) => dt.onRead === false && dt.user === 'admin').length,
                        data:  Object.values(item.messages).pop()
                    }])

                });

            }
        })


        return () => {
            setData([]);
        };


    }, [props.role])


    useEffect(() => {
        // const updates: any = {};
        // updates['/groups/' + props.uniqueID + '/name'] = 'Family';
        // update(ref(database), updates).then(() => console.log('Successful')).catch(err => console.log(err));
    }, []);


    return (
        <Cage className={'bg-gray-100 h-full left-pane'}>
            <Cage className="flex justify-between sr-only lg:not-sr-only ">
                <Paragraph text={'Chat'} className="text-xl font-bold text-gray-800  p-5" />
            </Cage>
            <SimpleBar className={'p-5 lg:mt-3 group-height'} style={{ maxHeight: '100%' }}>
                {
                    data.length ? Children.toArray(data.map((item, index) => (
                        <>
                            <button onClick={() => props.onChatListClick(item.name, item.uniqueID, 'chat')} className="border-b w-full my-2 py-2  cursor-pointer relative flex gap-4" >
                                <Avatar text={item.name.match(/\w{2}/gi)![0]} className="mr-2" />
                                <Cage className="flex-1 truncate">
                                    <Flexbox justifyContent="between">
                                        <Paragraph text={item.name} className="font-bold text-gray-800 text-left flex-1" />
                                        <Paragraph text={item.data!.timestamp!.match(/\s+\d{2}:\d{2}/gi)!.toString()} className="text-gray-800 text-right text-xs" />
                                    </Flexbox>
                                    <Flexbox justifyContent="between" alignItems="center">
                                        <Paragraph text={item.data!.message!} className=" truncate text-gray-600 text-left" />
                                        {/* <div className="bg-red-600 rounded-full w-5  h-5">
                                            <p className=" text-white text-center leading-5">{item.unRead}</p>
                                        </div> */}

                                    </Flexbox>
                                </Cage>
                            </button>
                        </>
                    ))) : <EmptyTag />
                }
            </SimpleBar>
        </Cage>
    )
}

const EmptyTag = () => (
    <Cage className="flex justify-center items-center  h-full  flex-col">
        <Paragraph text={'No chat  at the moment'} className={'text-gray-500 my-3'} />
    </Cage>
)


interface IChatList {
    onChatListClick: (name: string, uniqueID: string, type: string) => void;
    role: string;
}

export default ChatList;