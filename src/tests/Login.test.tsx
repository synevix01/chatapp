import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import Login from '../pages/Home';

test('renders title text', () => {
  render(
    <BrowserRouter >
      <Login />
    </BrowserRouter>);
  const linkElement = screen.getByText(/My PowerHouse/i);
  expect(linkElement).toBeInTheDocument();
});  
