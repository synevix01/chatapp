import { Paragraph } from "@synevix/react-widget";
import { IDatestamp } from "../utils/interface";
import dayjs from 'dayjs'; 


const Datestamp = (props: IDatestamp) => { 
    const currentDate = dayjs(formatDate(props!.currentDate!.match(/\d{2}\/\d{2}\/\d{4}/gi)!.toString()));  //year-month-day
    const today = dayjs().format('DD/MM/YYYY');
    const yesterday = dayjs().subtract(1, 'day').format('DD/MM/YYYY');
 
    if (props.index === 0) { 
        return <Paragraph text={today === props.currentDate.match(/\d{2}\/\d{2}\/\d{4}/gi)!.toString() ? 'Today' : yesterday === props.currentDate.match(/\d{2}\/\d{2}\/\d{4}/gi)!.toString() ? 'Yesterday' : props.currentDate.match(/\d{2}\/\d{2}\/\d{4}/gi)!.toString()} className="text-center text-gray-700  text-xs bg-gray-100 mx-auto mt-1 w-max px-3 py-1 rounded-full" />
    }
    else  { 
        const previousDate = dayjs(formatDate(Object.values(props.previousDate)[props.index - 1].timestamp.match(/\d{2}\/\d{2}\/\d{4}/gi)?.toString()));
        const difference = currentDate.diff(previousDate, 'day'); 
        if (difference > 0) {
            return <Paragraph text={today === props.currentDate.match(/\d{2}\/\d{2}\/\d{4}/gi)!.toString() ? 'Today' : yesterday === props.currentDate.match(/\d{2}\/\d{2}\/\d{4}/gi)!.toString() ? 'Yesterday' : props.currentDate.match(/\d{2}\/\d{2}\/\d{4}/gi)!.toString()} className="text-center text-gray-700  text-xs bg-gray-100 mx-auto  mt-1 w-max px-3 py-1 rounded-full" />
        }else{
            return null
        }
        
        
    }
   
 
}

const formatDate = (date?: string) => {
    const word = date!.split('/');
    return `${word[2]}-${word[1]}-${word[0]}`;
}
export default Datestamp;


