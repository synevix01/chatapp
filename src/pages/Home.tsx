import { Cage, Container, Flexbox, Grid, Heading, Image, Paragraph, Icon } from "@synevix/react-widget";
import { Link } from "react-router-dom";
import feature1 from '../assets/feature1.png';
import academics from '../assets/academics.jpg';
import relationship from '../assets/relationship.jpg';
import friendship from '../assets/friendship.jpg';
import health from '../assets/health.jpg';
import career from '../assets/career.jpg';
import development from '../assets/development.jpg';
import logo from '../assets/logo.png';
import about from '../assets/connect.jpg';


const Home = () => {
    return (
        <Cage className={''}>
            <div className={'h-min-screen  flex flex-col'}>
                <Header />
                <Banner />

            </div>
            <About />
            <Purpose />
            <Quote />
            {/* <Conclusion /> */}
            <Footer />
        </Cage>
    )
}

const Header = () => {

    return (
        <Flexbox className="justify-between px-1 lg:px-24 py-2 items-center bg-slate-50" gap={'5'}>
            <Cage className="flex items-center gap-x-2">
                <Image source={logo} alt={'mypowerhouse'} className={'mx-auto w-16 lg:w-20'} />
                <Paragraph text={'My PowerHouse'} className={' font-bold text-xl text-neutral-800 text-center'} />
            </Cage>
            <Link to={'/login'} className="hidden lg:flex  rounded-md py-2 px-8 font-bold  text-white bg-sky-500 hover:bg-sky-700">JOIN</Link>
        </Flexbox>
    )
}

const Banner = () => (
    <Grid className={'flex-1 px-3 lg:px-24 pt-8 bg-slate-50 min-h-full '} gap={'12'} lg={'2'}>
        <Container className={'flex justify-center flex-col lg:px-6'}>
            <Heading type="H4" text={'Share your thought'} className={'text-gray-700 mb-6 capitalize text-3xl lg:text-6xl font-bold'} />
            <Paragraph className={'mb-7  text-gray-500 font-medium  lg:text-lg'} text={'You got a personal assistance to help you with all your thought'} />
            <div>
                <Link to={"/login"} className={'hover:bg-sky-800 font-bold lg:text-lg text-white inline-block py-2 px-8 bg-sky-500 rounded-md'}>Join Now</Link>
            </div>
        </Container>
        <Container className={'flex justify-center '}>
            <Image alt={'Welcome'} source={feature1} height={'100%'}   className={''} />
        </Container>

    </Grid>
)

const About = () => (

    <Cage className="pt-12 px-3 lg:px-24 h-min-screen  ">
        <Paragraph text={'About'} className="uppercase font-bold text-2xl text-gray-600 text-center" />
        <Grid className={'my-12 '} lg={'2'} gap={'4'}>
            <Container className={' '}>
                <img alt={'About'} src={about} className={'mx-auto '} width={'100%'} />
            </Container>
            <Container className={'flex flex-col gap-y-6 justify-center text-zinc-700'}>
                <Paragraph className=" font-normal" text={'My Powerhouse  is an online messaging platform where users can anonymously share whatever issues they are going through.'} />
                <Paragraph className=" font-normal" text={'Users can receive replies from peer counsellors and other well trained officers.'} />
                <Paragraph className=" font-normal" text={'Share your problems and get solutions without your identity ever being disclosed.'} />
            </Container>


        </Grid>
    </Cage>
)

const Purpose = () => (
    <Cage className="h-min-screen px-3 lg:px-24 mt-12 lg:mt-0">
        <Paragraph text={'Why should i join my powerhouse'} className="uppercase font-bold text-gray-600 text-2xl text-center col-span-3 mb-6" />
        <Paragraph text={'Share whatever issues in the following areas and more'} className={'text-center text-zinc-600 text-lg'} />
        <Grid className={'mt-8'} gap={'7'} lg={'3'}>
            <Cage className={'border shadow-lg cursor-pointer rounded-md '} >
                <Image source={academics} alt={"academics"} height={'100%'} />
                <Heading type="H4" text={'Academics'} className={'font-semibold px-3 py-2'} />
                <Paragraph text={'Education is the main building block of a happy, fruitful, and productive life.'} className={'p-3 text-gray-700'} />
            </Cage>
            <Cage className={'border shadow-lg cursor-pointer rounded-md'} >
                <Image source={relationship} alt={"relationship"} />
                <Heading type="H4" text={'Relationship'} className={'font-semibold px-3 py-2'} />
                <Paragraph text={'Having someone wonder where you are when you don\'t come home at night is a very old human need'} className={'p-3 text-gray-700'} />
            </Cage>
            <Cage className={'border shadow-lg cursor-pointer rounded-md'} >
                <Image source={friendship} alt={"friendship"} />
                <Heading type="H4" text={'Friendship'} className={'font-semibold px-3 py-2'} />
                <Paragraph text={'The most beautiful discovery true friends make is that they can grow separately without growing apart.'} className={'p-3 text-gray-700'} />
            </Cage>
            <Cage className={'border shadow-lg cursor-pointer rounded-md'} >
                <Image source={health} alt={"health"} />
                <Heading type="H4" text={'Health'} className={'font-semibold px-3 py-2'} />
                <Paragraph text={'Time and health are two precious assets that we don\'t recognize and appreciate until they have been depleted'} className={'p-3 text-gray-700'} />
            </Cage>
            <Cage className={'border shadow-lg cursor-pointer rounded-md'} >
                <Image source={career} alt={"career"} />
                <Heading type="H4" text={'Career'} className={'font-semibold px-3 py-2'} />
                <Paragraph text={'Find out what you like doing best, and get someone to pay you for doing it.'} className={'p-3 text-gray-700'} />
            </Cage>
            <Cage className={'border shadow-lg cursor-pointer rounded-md'} >
                <Image source={development} alt={"development"} />
                <Heading type="H4" text={'Development'} className={'font-semibold px-3 py-2'} />
                <Paragraph text={'What you do makes a difference, and you have to decide what kind of difference you want to make.'} className={'p-3 text-gray-700'} />
            </Cage>

        </Grid>
    </Cage>
)

const Quote = () => (
    <Cage className="mt-12 mb-8 px-3 lg:px-24">
        <Paragraph text={'quotes'} className="uppercase font-bold text-gray-600 text-2xl text-center col-span-3 mb-6" />

        <Grid className={''} gap={'7'} md={'2'}>
            <Container className={'flex-1 bg-gray-200 p-5 rounded-md'}>
                <Icon name={'icon-quotes-left'} className={'text-4xl'} />
                <Paragraph className={'mb-7'} text={'Only through difficulty can you rest at the peak, look out over the horizon that surrounds you, and rejoice beyond today\'s imagination at how beautiful life really is and how lucky we truly are'} />
                <Paragraph text={'~~Michelle C. Ustaszeski'} className="italic text-right" />
            </Container>
            <Container className={'flex-1  bg-gray-200 p-5 rounded-md'}>
                <Icon name={'icon-quotes-left'} className={'text-4xl'} />
                <Paragraph className={'mb-7'} text={'Who amongst has not enjoyed looking at a beautiful sunset or a sunrise! We all have at some point of time stopped to admire a snow covered landscape or a beautiful sunny day.'} />
                <Paragraph text={'~~Pegasus, Natural Wonders'} className="italic text-right" />


            </Container>

        </Grid>
    </Cage>
)



const Footer = () => (
    <footer className="bg-black text-white px-3 lg:px-24 py-3 lg:flex justify-around" >
        <p>Copyright &#169; {new Date().getFullYear()}  </p>
        <Paragraph text={'Designed by Synevix'} />
    </footer>
)

export default Home;