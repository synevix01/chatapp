import { Button, Cage, Grid, Paragraph, TextField, Image } from "@synevix/react-widget";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import {   useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import login from '../assets/login.svg';
import logo from '../assets/logo.png';
import { IUser, IError } from "../utils/interface";


const Login = () => {

    const [user, setUser] = useState<IUser>({} as IUser);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [error, setError] = useState<IError>({ status: false, message: '' });
    const auth = getAuth();
    const history = useNavigate();

   
    const onJoin = () => {
        setIsLoading(true);
        setError({ status: false, message: '' });


        //history({ pathname: '/dashboard' });  // Remove later

        if (user.passcode?.length && user.username?.length) {
            signInWithEmailAndPassword(auth, user.username + '@mph.com', user.passcode)
                .then((userCredential) => {
                    // Signed in 

                    setIsLoading(false);
                    history({ pathname: '/dashboard' });
                })
                .catch((error) => {
                    console.log(error);
                    setIsLoading(false);
                    setError({ status: true, message: error.code })
                });

        } else {
            setError({ status: true, message: 'Please provide valid information' });
            setIsLoading(false)
        }
        return;
    }


    return (
        <Grid className="bg-blue-100 min-h-screen " lg="2" gap="4">
            <Cage className="sr-only lg:not-sr-only p-3   h-full">
                <Link to={'/'} className={'p-6   inline-block '}>
                    <img src={logo} alt={'logo'} className={'w-20 mx-auto'} />
                    <Paragraph text={'My PowerHouse'} className={'font-bold text-xl text-gray-800'} />
                </Link>
                <img src={login} alt="login" className="p-12" />
            </Cage>
            <Cage className="py-8 bg-gray-700 flex flex-col justify-center px-4 md:px-12" >
                <Image source={logo} alt={'mypowerhouse'} className={'mx-auto'} width={'150px'} />
                <Paragraph text={'mPh'} className="hidden w-36 h-36 mx-auto border-2 rounded-full px-8 py-12 logo_text text-4xl font-bold text-white" />
                <Paragraph text={'My PowerHouse'} className="my-4 text-xl font-bold text-white text-center" />
                <Grid gap="6" className="">
                    <Cage className="w-full lg:w-1/2 mx-auto ">
                        <Paragraph text={'Username'} className="text-white mb-1" />
                        <TextField required={true} className="w-full" type="text" placeholder={'Enter username'} value={user.username} onValueChange={(e) => setUser({ ...user, username: e.target.value })} />

                    </Cage>
                    <Cage className="lg:w-1/2 mx-auto w-full">
                        <Paragraph text={'Password'} className="text-white mb-1" />
                        <TextField className="w-full" type="password" showEye={true} placeholder={'Enter password'} value={user.passcode} onValueChange={(e) => setUser({ ...user, passcode: e.target.value })} />
                        
                    </Cage>
                    <Paragraph text={error.message} className={`text-red-400 italic ${error.status || 'hidden'} lg:w-1/2 mx-auto w-full`} />
                    <Button isLoading={isLoading} onClick={onJoin} text={'Login'} className="lg:w-1/2 mt-2 mx-auto w-full text-white py-2 px-5 font-bold button-bg hover:bg-blue-500 outline-none" />
                    <p className={`text-white lg:w-1/2 mx-auto w-full`} >
                        Don't have an account?
                        <Link to={'/register'} className="text-blue-400 pl-2">Register Here</Link>

                    </p>
                </Grid>



            </Cage>
        </Grid>
    )
}

export default Login;