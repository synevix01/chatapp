import { Cage, Paragraph, Avatar, Flexbox, Button } from "@synevix/react-widget";
import { Children, useEffect, useState } from "react";
import SimpleBar from 'simplebar-react';
import { database, onValue, ref,   update } from '../utils/firebase';

const User = (props: IUser) => {

    const [data, setData] = useState<any[]>([]);
    const [contextMenu, setContextMenu] = useState<number>(-1);


    useEffect(() => {

        document.getElementById('bodyHolder')?.addEventListener('click', (e) => setContextMenu(-1))
        document.getElementById('chat-book')?.addEventListener('click', (e) => setContextMenu(-1))


    })


    useEffect(() => {

        onValue(ref(database, 'user'), (res: any) => {
            if (res.val() != null) {
                setData([])
                Object.values(res.val()).forEach((item: any) => {
                    setData((prev) => [...prev, { name: item.username, uniqueID: item.uniqueID, lastSeen: item.lastSeen, role: item.role }])

                });

            }
        })

        return () => {
            setData([]);
        };


    }, [])


    useEffect(() => {
        //window.scrollTo(0, document.body.scrollHeight);
    });

    // const onUserDelete = (id: string) => {

    //     remove(ref(database, `user/${id}`));
    // }

    const onAddAdmin = (id: string) => {
        const updates: any = {};
        updates['/user/' + id + '/role'] = 'admin';
        update(ref(database), updates).then(() => console.log('Successful')).catch(err => console.log(err));
    }

    const onRemoveAdmin = (id: string) => {
        const updates: any = {};
        updates['/user/' + id + '/role'] = 'guest';
        update(ref(database), updates).then(() => console.log('Successful')).catch(err => console.log(err));
    }

    return (
        <Cage className={'bg-gray-100  h-full left-pane'}>
            <Cage className="flex justify-between sr-only lg:not-sr-only">
                <Paragraph text={'User'} className="text-xl font-bold text-gray-800  p-5" />
            </Cage>
            <SimpleBar className={'p-5 lg:mt-3 group-height'} style={{ maxHeight: '100%' }}>
                {
                    data.length ? Children.toArray(data.map((item, index) => (
                        <>
                            <div className="border-b w-full my-2 py-2  cursor-pointer relative  flex gap-4 items-center" >
                                <div className="relative">
                                    <Avatar text={item.name && item.name.match(/\w{2}/gi)![0]} className="mr-2" />
                                    <p className={`w-3 h-3 ${item.lastSeen === 'online' ? 'bg-green-600' : 'bg-yellow-600'} border rounded-full absolute right-2 bottom-1 `}  ></p>

                                </div> 
                                <Cage className="flex-1 truncate">
                                    <Flexbox justifyContent="between" >
                                        <Paragraph text={`${item.name}   ${item.role === 'admin' ?  '(admin)' : '' }` } className={`font-bold text-gray-800 text-left flex-1`} />
                                        <Button onClick={() => setContextMenu((prev) => prev === index ? -1 : index)} icon={'icon-more'} className={`bg-transparent  ${item.name === props.username ? 'hidden ' : 'null '}`} />
                                    </Flexbox>
                                </Cage>
                                <div className={`${contextMenu === index ? 'null' : 'hidden'}    bg-gray-50 w-max p-3 absolute right-4 shadow-md z-20 rounded top-4`}>
                                    {
                                        item.role === 'admin' 
                                        ? <Button icon="icon-plus3" text="Remove Admin" onClick={() => onRemoveAdmin(item.uniqueID)} className={'bg-transparent  w-full  text-left '} style={{display: 'block'}}/>
                                        : <Button icon="icon-plus3" text="Add Admin" onClick={() => onAddAdmin(item.uniqueID)} className={'bg-transparent  w-full  text-left '} style={{display: 'block'}}/> 
                                    }
                                    
                                    {/* <Button icon="icon-trash" text="Delete  User" onClick={() => onUserDelete(item.uniqueID)} className={'block w-full text-left my-2'} /> */}
                                </div>

                            </div>
                        </>
                    ))) : <EmptyTag />
                }
            </SimpleBar>
        </Cage>
    )
}

const EmptyTag = () => (
    <Cage className="flex justify-center items-center h-full flex-col">
        <Paragraph text={'No user  at the moment'} className={'text-gray-500 my-3'} />

    </Cage>
)


interface IUser {
    onUserClick: (name: string, uniqueID: string, type: string) => void;
    username: string;
}

export default User;