import { Cage, Paragraph, Avatar, Flexbox } from "@synevix/react-widget";
import { useEffect, useState } from "react";
import SimpleBar from 'simplebar-react';
import { database, onValue, push, ref } from '../utils/firebase';
import _ from 'lodash';

const Direct = (props: IDirect) => {
    const [data, setData] = useState<IDirectData>({} as IDirectData);
    const uniqueID = push(ref(database, `direct`)).key;


    useEffect(() => {

        onValue(ref(database, 'direct/' + props.username), (res: any) => {

            if (res.val() != null) {
                if (_.has(res.val(), 'messages')) {
                    const item = res.val() as IChatCast;
                    const result = { name: item.user, uniqueID: item.uniqueID, data: Object.values(item.messages).pop() as any }
                    setData(result);
                }


            }
        })

        return () => {
            setData({} as IDirectData)
        }


        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])



    return (
        <Cage className={'bg-gray-100  h-full  left-pane'}>
            <Cage className="flex justify-between sr-only lg:not-sr-only">
                <Paragraph text={'Direct'} className="text-xl font-bold text-gray-800 p-5" />

            </Cage>


            <SimpleBar className={'p-5 lg:mt-3 group-height'} style={{ maxHeight: '100%' }}>
                {

                    _.isEmpty(data) ?
                        <Cage className="flex justify-center items-center">
                            <button onClick={() => props.onDirectClick(props.username, uniqueID!, 'direct')} className="border p-3 rounded-md bg-gray-500 text-white ">Click here to start a new chat</button>
                        </Cage>
                        :
                        <button onClick={() => props.onDirectClick(data.name, data.uniqueID, 'direct')} className="border-b w-full my-2 py-2  cursor-pointer relative  flex gap-4" >
                            <Avatar text={'MPH'} className="mr-2" />
                            <Cage className="flex-1 truncate">
                                <Flexbox justifyContent="between">
                                    <Paragraph text={'My PowerHouse'} className="font-bold text-gray-800 text-left flex-1" />
                                    <Paragraph text={data.data!.timestamp!.match(/\s+\d{2}:\d{2}/gi)!.toString()} className="text-gray-800 text-right text-xs" />
                                </Flexbox>
                                <Paragraph text={data!.data! && data.data.message} className="truncate text-gray-600 text-left" />
                            </Cage>
                        </button>


                }

            </SimpleBar>


        </Cage>
    )
}


interface IDirect {
    onDirectClick: (name: string, uniqueID: string, type: string) => void;
    username: string;
}

interface IDirectData {
    name: string;
    uniqueID: string;
    data: IData
}

interface IData {
    timestamp: any;
    message: string;
}

interface IChatCast {
    user: string;
    uniqueID: string;
    messages: any;
}
export default Direct;

