export interface IChat {
    message: string;
    user: string;
    timestamp: string;
    onRead: boolean;
    uniqueID: string;
    name: string;
    reply: string | null;
    replyUser: string | null;
} // 0244902323 --- Amusa

export interface IChats {
    name: string;
    uniqueID: string;
    username: string;
    type: string;
    role: string;
    goBack: (arg0: boolean) => void;
}


export interface IDatestamp {
    index: number;
    previousDate: IChat[];
    currentDate: string;
}

export interface IMessagebox {
    onSend: (arg0: IChat) => void;
    onReplyAction: (arg0: boolean) => void;
    username: string;
    reply: IReply;
}

export interface IUser {
    lastSeen: string;
    dateCreated: string;
    uniqueID: string | null;
    username: string;
    passcode: string;
    role: string;
    id: string;
}

export interface IError {
    status: boolean;
    message: string;
}

export interface IGroupData {
    name: string;
    description: string;
    timestamp: string;
    uniqueID: string;
    messages: any;
}  

export interface IReply {
    message: string;
    user: string;
    status: boolean;
}