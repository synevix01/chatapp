import { Grid, Label, TextField, TextArea, Button, Paragraph } from "@synevix/react-widget";

import { useState } from "react";
import { database, push, ref, update } from "../utils/firebase";
import { IError, IGroupData } from "../utils/interface";
import { alphaNumber, checkLength, isEmpty, textboxFilter } from '../utils/textboxFilter';

import Modal from "./Modal";

const CreateGroup = (props: ICreateGroup) => {
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [group, setGroup] = useState<IGroupData>({} as IGroupData)
    const [error, setError] = useState<IError>({} as IError);

    const onBtnGroupCreate = () => {
        setIsLoading(true);
        const updates: any = {};
        const uniqueID = push(ref(database, `groups`)).key!;
        const uniqueMessageID = push(ref(database, `groups/${uniqueID}`)).key!;
        group.timestamp = new Date().toLocaleString('en-GB');
        group.uniqueID = uniqueID;
        group.messages = {
            [uniqueMessageID]: {
                message: group.description,
                onRead: false,
                timestamp: group.timestamp,
                uniqueID: uniqueMessageID,
                user: 'admin'

            }
        }

        textboxFilter(group.name,)
            .then(e => isEmpty(e))
            .then(e => checkLength(e))
            .then(e => alphaNumber(e))
            .then(() => isEmpty(group.description))
            .then((e) => checkLength(e))
            .then(() => {
                updates['/groups/' + uniqueID] = { ...group };
                update(ref(database), updates).then(() => { 
                    setIsLoading(false);
                    props.setModal(false);
                    setGroup({} as IGroupData);
                }).catch(err => { setIsLoading(false); Promise.reject(err.code) });
            })
            .catch((e: string) => {
                setError({ message: e.replace(/Username/gi, 'TextField'), status: true });  
                setIsLoading(false);
            });





    }


    return (
        <div className="relative z-50">
            <Modal isOpen={props.modal} setOpen={() => props.setModal(!props.modal)} title={'Create New Group'}  >
                <Grid gap="4">
                    <Label className="text-gray-600">Group Name</Label>
                    <TextField placeholder={'Group Name'} value={group.name} onValueChange={(e) => setGroup({ ...group, name: e.target.value })} />
                    <Label className="text-gray-600">Description</Label>
                    <TextArea placeholder="Description" value={group.description} onValueChange={(e) => setGroup({ ...group, description: e.target.value })} />
                    <Paragraph text={error.message} className={`text-red-400 italic ${error.status || 'hidden'} lg:w-1/2 mx-auto w-full`} />
                    <Button isLoading={isLoading} onClick={onBtnGroupCreate} text={'Create Group'} className={'bg-gray-800 hover:bg-gray-700 text-white p-2'} />
                </Grid>
            </Modal>
        </div>

    )
}

interface ICreateGroup {
    modal: boolean;
    setModal: (args: boolean) => void;
}


export default CreateGroup;