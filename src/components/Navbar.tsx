import { Cage, Flexbox, Icon, Span } from "@synevix/react-widget";
import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { database, onValue, ref } from "../utils/firebase";

const Navbar = () => {
    const username = window.localStorage.getItem('username');
    //const navigate = useNavigate();
    const [, setUnRead] = useState<any>(0)
    const location = useLocation();

    // const signOut = () => {
    //     window.localStorage.removeItem('username');
    //     navigate({ pathname: '/account' }, { replace: true });

    // }

    useEffect(() => {
        onValue(ref(database, `personal/${username}/`), (result: any) => {

            if (result.val() != null) {
                setUnRead(Object.values(result.val()).filter((i: any) => i.onRead === false && i.user !== username).length)
            }
        });

    }, [username])

    return (
        <Cage className="p-3 pb-1 chat_navbar text-white sticky inset-0 bottom-unset z-50">
            <Flexbox className={''} justifyContent={'between'}>
                <Link to={'/group-chat'} className={`${location.pathname === '/group-chat' && 'border-b'} text-white font-bold flex-1 p-3 text-center`}>
                    <Icon name={'icon-users4'} className={'mx-2'} />
                    Group
                </Link>
                {
                    username !== 'admin' ?
                        <Link to={'/personal-chat'} className={`${location.pathname === '/personal-chat' && 'border-b'} flex-1 p-3 text-center font-bold`}>
                            <Icon name={'icon-users'} className={'mx-2'} />
                            Personal
                            <Span className="p-1 rounded-full ml-2 text-xs"></Span>
                        </Link> :
                        <Link to={'/chat-list'} className={`${location.pathname === '/chat-list' && 'border-b'} flex-1 p-3 text-center font-bold `}>
                            <Icon name={'icon-users'} className={'mx-2'} />
                            Chat List
                        </Link>
                }

            </Flexbox>
        </Cage>
    )
}

export default Navbar;


