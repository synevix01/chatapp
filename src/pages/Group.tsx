import { Cage, Paragraph, Avatar, Button, Flexbox } from "@synevix/react-widget";
import { Children, useEffect, useState } from "react";
import SimpleBar from 'simplebar-react';
import CreateGroup from "../components/CreateGroup";
import { database, onValue, ref } from '../utils/firebase';
import { IGroupData } from "../utils/interface";

const Group = (props: IGroup) => {
    const [data, setData] = useState<any[]>([]);
    const [modal, setModal] = useState<boolean>(false);

    useEffect(() => {

        onValue(ref(database, 'groups'), (res: any) => {
            if (res.val() != null) {
                setData([])
                Object.values(res.val() as IGroupData[]).forEach((item: IGroupData) => {
                    setData((prev) => [...prev, { name: item.name, uniqueID: item.uniqueID, data: item.messages && Object.values(item.messages).pop() }])
                });
            }
        })

        return () => {
            setData([]);
        };

    }, [])



    return (
        <Cage className={'bg-gray-100  h-full left-pane '}>
            <Cage className="my-4 flex justify-between sr-only lg:not-sr-only ">
                <Paragraph text={'Group'} className="text-xl font-bold text-gray-800 p-5" />
                {
                    props.role === 'admin' &&
                    <Button icon="icon-plus3" onClick={() => setModal(true)} className={'text-gray-800 font-bold p-5 bg-transparent '} />
                }


            </Cage>
            <CreateGroup modal={modal} setModal={(args: boolean) => setModal((() => args))} />


            <SimpleBar className={'p-5 lg:mt-3 group-height'} style={{ maxHeight: '100%' }}>
                {
                    data.length ? Children.toArray(data.map((item) => (
                        <button onClick={() => props.onGroupClick(item.name, item.uniqueID, 'groups')} className="border-b w-full py-2 cursor-pointer relative flex mb-5 gap-4" >
                            <Avatar text={ item.name.match(/\w{2}/gi)![0]} className="mr-2" />
                            <Cage className="flex-1 truncate">
                                <Flexbox justifyContent="between">
                                    <Paragraph text={item.name} className="font-bold text-gray-800 text-left flex-1" />
                                    <Paragraph text={item.data &&  item.data!.timestamp!.match(/\s+\d{2}:\d{2}/gi)!.toString()} className="text-gray-800 text-right text-xs" />
                                </Flexbox>

                                <Paragraph text={item.data && item.data!.message!} className="truncate text-gray-600 text-left" />
                            </Cage>
                        </button>

                    ))) : <EmptyTag role={props.role} setModal={(args) => setModal(args)} />
                }
            </SimpleBar>



        </Cage>
    )
}

const EmptyTag = ({ role, setModal }: { role: string, setModal: (args: boolean) => void }) => (
    <Cage className="flex justify-center items-center fh-full flex-col">
        <Paragraph text={'No available group at the moment'} className={'text-gray-500 my-3'} />
        {
            role === 'admin' && <Button icon="icon-plus3" onClick={() => setModal(true)} text={'Click to create a group'} className={'bg-transparent rounded-md border p-3 bg-gray-500 text-white'} />
        }
    </Cage>
)

interface IGroup {
    onGroupClick: (name: string, uniqueID: string, type: string) => void;
    role: string;
}

export default Group;
