export const textboxFilter = (word: string) => {
    
    return Promise.resolve(word)
    
}

export const isEmpty = (word: string) => {
    if (word !== undefined || null || '') {
        return Promise.resolve(word);
    }else {
        return Promise.reject('Username cannot be empty')
    }
}

export const noSpace = (word: string) => {
    if (!word.match(/\s/gi)) {
        return Promise.resolve(word);
    }else {
        return Promise.reject('Username cannot include space')
    }
}


export const checkLength = (word: string) => {
    if (word.length > 3) {
        return Promise.resolve(word.trim())
    }else {
        return Promise.reject('Username is too small')
    }
}

export const alphaNumber = (word: string) => {
    if (!word.match(/\W+/gi) && !word.match(/^[0-9]/gi)) {
        return Promise.resolve(word)
    }else {
        return Promise.reject('Invalid Username')
    }
}

export const existence = (word: string) => {
    if (!word.match(/admin|bots|bot/gi)) {
        return Promise.resolve(word)
    }else {
        return Promise.reject('Username is unavailable') 
    }
}